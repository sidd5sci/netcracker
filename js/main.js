

$(document).ready(function(){

  // dropwond 
  $("#user").on("click",function(){
    var block = $("#dropdown-user");
    var state = block.attr("state");
    console.log(state);
    if(state == 'show'){
      block.hide();
      block.attr("state","hide");
    }
    else{
      block.show();
      block.attr("state","show");
    }
  });

  // toggle side menu

  $(".collapse").on("click",function(){
    var block = $(".left-bar");
    var state = block.attr("state");
    console.log(state);
    if(state == 'expanded'){
      block.attr("state","shrinked");
      block.css("width","7%");
      $(".content-main").css("width","93%");
      $(".content-main").css("left","7%");
      $(".list-item a").addClass("shrinked");

    }
    else{
      block.attr("state","expanded");
      block.css("width","15%");
      $(".content-main").css("width","85%");
      $(".content-main").css("left","15%");
      $(".list-item a").removeClass("shrinked");
    }
  });



  $("#dashboard").on("click",function(){
    console.log("check");
    $.get("http://localhost/api/dashboard.php",function(result){
        $(".content").html(result);
        console.log(result);
    });
  });
  $("#notification").on("click",function(){
    console.log("check");
    $.get("http://localhost/api/notification.php",function(result){
        $(".content").html(result);
        console.log(result);
    });
  });
  $("#profile").on("click",function(){
    console.log("check");
    $.get("http://localhost/api/profile.php",function(result){
        $(".content").html(result);
        console.log(result);
    });
  });
  $("#settings").on("click",function(){
    console.log("check");
    $.get("http://localhost/api/settings.php",function(result){
        $(".content").html(result);
        console.log(result);
    });
  });


  /// realtime search 
  var Timer,value;
  $("#search-box").keyup(function(){
      value = $(this).val();

      clearTimeout(Timer);
      Timer = setTimeout(addData,700);
    
    
  });

  function addData(){
    $.post("http://localhost/api/getProducts.php",{keyword:value},function(result){
        var obj = JSON.parse(result);
        var count = obj.length;
        var list = $(".search-results");
        console.log(obj[0]);
        list.html("");
        for(var i =0;i<count && i<3;i++){
          list.append(
            `
            <li class="search-item">
                <div class="row">
                    <div class="col-md-3">
                        <img src="http://localhost/websites/befreshVeg.com/assets/images/products/`+obj[i].featured_image+`" class="search-img"/>
                    </div>
                    <div class="col-md-9">
                        <h4>&nbsp; `+obj[i].name+`</h4>
                    </div>
                </div>
            </li>
            `
          );
        }
        if(value == ""){
          list.html("");
        }
    });
  
  }




});